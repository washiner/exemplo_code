import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static const double _sizeInicial = 100.0;

  double _size = _sizeInicial;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
          onTap: crescer,
          onDoubleTap: diminuir,
          onLongPress: resetar,
          child: Container(
            width: _size,
            height: _size,
            color: Colors.green,
          ),
        ),
      ),
    );
  }

  void crescer() {
    setState(() {
      _size += 10.0;
    });
  }

  void diminuir() {
    setState(() {
      _size -= 10.0;
    });
  }

  void resetar() {
    setState(() {
      _size = _sizeInicial;
    });
  }
}
